Before running any scripts please cd into the root of the project folder.

To build the compiler run the script named BuildCompiler.sh

To run the compiler run RunCompiler.sh. This bash file takes the name of the source file as its argument.
Source files included with this project are contained in SourceCodeForCompiler. The output from these files
when executed is contained in SourceCodeForCompiler/OutputTextFiles.

When compilation is successful the compiler generates output.j in the bin folder in the root of the project.

To compile output.j into a class file and run it you use ExecuteJasminCode.sh

This will compile output.j into a java class file and then execute the class file.