int square(int x){
	return x * x;
}

float half(float f){
	return f / 2.0;
}

string returnStr(){
	return "test";
}

int main(){
	int x = square(5);
	
	printf("x=%d\n", x);
	float f = 11.5;
	printf("f/2=%f\n", half(f));
	printf("Function returned string: %s", returnStr());
}