/* Function to merge the two haves arr[l..m] and arr[m+1..r] of array arr[] */
void merge(int[] arr, int l, int m, int r)
{
	int i;
	int j;
	int k;
	int n1 = m - l + 1;
	int n2 = r - m;

	/* create temp arrays */
	int L[n1];
	int R[n2];

	/* Copy data to temp arrays L[] and R[] */
	for (i = 0; i < n1; i = i + 1){
		L[i] = arr[l + i];
	}
	for (j = 0; j < n2; j = j + 1){
		R[j] = arr[m + 1 + j];
	}

	/* Merge the temp arrays back into arr[l..r]*/
	i = 0;
	j = 0;
	k = l;
	while (i < n1 && j < n2)
	{
		if (L[i] <= R[j])
		{
			arr[k] = L[i];
			i = i + 1;
		}
		else
		{
			arr[k] = R[j];
			j = j + 1;
		}
		k = k + 1;
	}

	/* Copy the remaining elements of L[], if there are any */
	while (i < n1)
	{
		arr[k] = L[i];
		i = i + 1;
		k = k + 1;
	}

	/* Copy the remaining elements of R[], if there are any */
	while (j < n2)
	{
		arr[k] = R[j];
		j = j + 1;
		k = k + 1;
	}
}

/* l is for left index and r is right index of the sub-array
of arr to be sorted */
void mergeSort(int[] arr, int l, int r)
{
	if (l < r)
	{
		int lplusr = r + l;
		int m = lplusr / 2; //Same as (l+r)/2, but avoids overflow for large l and h
		mergeSort(arr, l, m);
		mergeSort(arr, m + 1, r);
		merge(arr, l, m, r);
	}
}

void initializeArray(int[] array, int arraySize){
	int i;
	int j = 0;
	for (i = arraySize - 1; i >= 0; i = i - 1){
		if (i % 2 == 0){
			array[j] = i * 6;

		}
		else{
			array[j] = i;
		}
		j = j + 1;
	}
}

void printArray(int[] array, int size){
	int i;
	for (i = 0; i < size; i = i + 1){
		printf("%d ", array[i]);
	}
}

string introMessage(){
	return "Program to demonstrate merge sort by team Functionally Recursive";
}

int main()
{
	printf(introMessage());

	int arrSize = 10;

	do{
		int array[arrSize];
		printf("\n\nArray Size: %d\n\n", arrSize);
		initializeArray(array, arrSize);

		printf("Unsorted:\n");
		printArray(array, arrSize);

		mergeSort(array, 0, arrSize - 1);

		printf("\nSorted:\n");
		printArray(array, arrSize);

		arrSize = arrSize + 10;
	} while (arrSize <= 50);
}