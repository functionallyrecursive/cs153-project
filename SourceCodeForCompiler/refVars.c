void refVariables(int & x, float & f, string & s){
	x = 5;
	f = 3.5;
	s = "RefVar";
}

void printVariables(int x, float f, string s){
	printf("Variable values s=%s x=%d f=%f\n\n", s, x, f);
}

int main(){
	int x;
	float f;
	string s;
	
	printf("Before\n");
	printVariables(x, f, s);
	refVariables(x,f,s);
	printf("After\n");
	printVariables(x, f, s);
	
}