java -classpath src/main/java/javacc.jar jjtree -NODE_EXTENDS=intermediate.icodeimpl.ICodeNodeImpl -JJTREE_OUTPUT_DIRECTORY=src/main/java/frontend src/main/java/CScanner.jjt
java -classpath src/main/java/javacc.jar javacc -OUTPUT_DIRECTORY=src/main/java/frontend src/main/java/frontend/CScanner.jj

mkdir bin

javac src/main/java/frontend/CScanner.java -d bin -cp src/main/java
javac src/main/java/runtime/* -d bin -cp src/main/java
