package intermediate.types;

public enum TypeImpl {
	CHAR, FLOAT, STRING, INT, FUNCTION, VOID;
}
