package intermediate.util;

import java.util.ArrayList;

import intermediate.SymTab;

public class StackPrinter {
	private static CrossReferencer crossref = new CrossReferencer();
	public ArrayList<SymTab> tables;

	public StackPrinter() {
		tables = new ArrayList<>();
	}

	public void addStack(SymTab s) {
		tables.add(s);
	}

	public void print() {
		for (SymTab s : tables)
			crossref.printSymTab(s);
	}

}
