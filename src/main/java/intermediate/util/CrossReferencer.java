package intermediate.util;

import java.util.ArrayList;

import intermediate.SymTab;
import intermediate.SymTabEntry;
import intermediate.SymTabStack;
import intermediate.types.TypeImpl;

/**
 * <h1>CrossReferencer</h1>
 *
 * <p>
 * Generate a cross-reference listing.
 * </p>
 *
 * <p>
 * Copyright (c) 2009 by Ronald Mak
 * </p>
 * <p>
 * For instructional purposes only. No warranties.
 * </p>
 */
public class CrossReferencer {
	private static final int NAME_WIDTH = 16;

	private static final String NAME_FORMAT = "%-" + NAME_WIDTH + "s";
	private static final String NUMBERS_LABEL = " Line numbers    ";
	private static final String NUMBERS_UNDERLINE = " ------------    ";
	private static final String NUMBER_FORMAT = " %03d";

	private static final String TYPE_LABEL = " Type    ";
	private static final String TYPE_UNDERLINE = " ----    ";
	private static final int TYPE_WIDTH = 8;
	private static final String TYPE_FORMAT = " %-" + TYPE_WIDTH + "s";

	private static final String RTYPE_LABEL = " Return Type  ";
	private static final String RTYPE_UNDERLINE = " -----------  ";
	private static final int RTYPE_WIDTH = 13;
	private static final String RTYPE_FORMAT = " %-" + RTYPE_WIDTH + "s";

	private static final int LABEL_WIDTH = NUMBERS_LABEL.length();
	private static final int INDENT_WIDTH = NAME_WIDTH + LABEL_WIDTH;

	private static final StringBuilder INDENT = new StringBuilder(INDENT_WIDTH);

	static {
		for (int i = 0; i < INDENT_WIDTH; ++i)
			INDENT.append(" ");
	}

	// /**
	// * Print the cross-reference table.
	// * @param symTabStack the symbol table stack.
	// */
	// public void print(SymTabStack symTabStack)
	// {
	// System.out.println("\n===== CROSS-REFERENCE TABLE =====");
	// printColumnHeadings();
	//
	// printSymTab(symTabStack.getLocalSymTab());
	// }

	/**
	 * Print column headings.
	 */
	private void printColumnHeadings() {
		System.out.println();
		System.out.println(String.format(NAME_FORMAT, "Identifier") + TYPE_LABEL + RTYPE_LABEL + NUMBERS_LABEL);

		System.out.println(
				String.format(NAME_FORMAT, "----------") + TYPE_UNDERLINE + RTYPE_UNDERLINE + NUMBERS_UNDERLINE);
	}

	/**
	 * Print the entries in a symbol table.
	 * 
	 * @param SymTab
	 *            the symbol table.
	 */
	public void printSymTab(SymTab symTab) {
		System.out.println("\n===== CROSS-REFERENCE TABLE =====");
		printColumnHeadings();
		// Loop over the sorted list of symbol table entries.
		ArrayList<SymTabEntry> sorted = symTab.sortedEntries();
		for (SymTabEntry entry : sorted) {
			ArrayList<Integer> lineNumbers = entry.getLineNumbers();

			// For each entry, print the identifier name
			// followed by the line numbers.
			System.out.print(String.format(NAME_FORMAT, entry.getName()));
			System.out.printf(TYPE_FORMAT, entry.getType());
			TypeImpl rType = entry.getReturnType();
			String rTypeStr = (rType == null) ? "N/A" : rType.name();
			System.out.printf(RTYPE_FORMAT, rTypeStr);
			if (lineNumbers != null) {
				for (Integer lineNumber : lineNumbers) {
					System.out.print(String.format(NUMBER_FORMAT, lineNumber));
				}
			}
			System.out.println();
		}
	}
}
