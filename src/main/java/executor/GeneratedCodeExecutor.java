package executor;

import java.io.IOException;
import java.util.Scanner;

public class GeneratedCodeExecutor {
	private String filePath;

	
	public static final String COMPILE_JAVA_RUNTIME_COMMAND = "javac src/main/java/runtime/* -d .";
	public static final String COMPILE_COMMAND = "java -jar lib/jasmin.jar %s";
	public static final String RUN_COMMAND = "java -cp . CS153FunctionallyRecursive";

	public GeneratedCodeExecutor(String filePath) {
		this.filePath = filePath;
	}

	public void runProgram() {
		try {
			System.out.println("Compiling Code");
			System.out.println("=================");
			if (compileToClassFile() && compileJavaRuntimeClasses()) {
				System.out.println("=================");
				System.out.println("Running Compiled Code");
				System.out.println("=================");
				runCompiledCode();
				System.out.println("=================");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private boolean compileToClassFile() throws IOException {
		String command = String.format(COMPILE_COMMAND, filePath);
		return runCommand(command);
	}

	private void runCompiledCode() throws IOException {
		runCommand(RUN_COMMAND);
	}
	
	private boolean compileJavaRuntimeClasses() throws IOException {
		return runCommand(COMPILE_JAVA_RUNTIME_COMMAND);
	}

	private boolean runCommand(String command) throws IOException {
		System.out.println(command);
		Process p = Runtime.getRuntime().exec(command);

		Scanner std = new Scanner(p.getInputStream());
		Scanner err = new Scanner(p.getErrorStream());

		StringBuilder stdOut = new StringBuilder();
		StringBuilder errOut = new StringBuilder();

		while (std.hasNextLine() || err.hasNextLine()) {
			if (std.hasNextLine()) {
				stdOut.append(std.nextLine() + "\n");
			}
			if (err.hasNextLine()) {
				errOut.append(err.nextLine() + "\n");
			}
		}
		std.close();
		err.close();


		System.out.println("Standard Out:\n" + stdOut.toString());
		System.out.println("Error Out:\n" + errOut.toString());

		// returns true if no error output
		return errOut.toString().length() == 0;
	}
}
