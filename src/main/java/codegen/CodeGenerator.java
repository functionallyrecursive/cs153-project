package codegen;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import codegen.Function.Parameter;
import frontend.ErrorMessage;
import frontend.ErrorMessageHandler;
import intermediate.ICodeNode;
import intermediate.ICodeNodeType;
import intermediate.SymTabStack;
import intermediate.icodeimpl.ICodeKeyImpl;
import intermediate.icodeimpl.ICodeNodeTypeImpl;

public class CodeGenerator {

	public static final HashSet<ICodeNodeType> LOGICAL_OPERATORS = new HashSet<>();
	static{
		LOGICAL_OPERATORS.add(ICodeNodeTypeImpl.AND);
		LOGICAL_OPERATORS.add(ICodeNodeTypeImpl.OR);
	}
	
	// modified by any operation that uses the stack
	// used as a "high watermark" for the currentStackNeeded
	private int stackLimit;
	private int currentStackNeeded;
	private int numOfLocalVars;
	private int variablePosition;
	
	private boolean hasMain;
	
	// used to create unique labels
	private int labelNum;
	private HashMap<String, Variable> variableLookupTable;
	private HashMap<String, Function> functionLookupTable;

	private ICodeNode root;
	private ErrorMessageHandler errors;
	private StringBuilder contentsOfFile;
	
	//used for return
	private Function currentFunction;

	// puts output file in working directory
	public static final String OUTPUT_FILE_PATH = new File("").getAbsolutePath() + "/bin/output.j";

	// default heading for all programs
	public static final String CLASS_NAME = "CS153FunctionallyRecursive";
	public static final String DEFAULT_HEADER = ".class public " + CLASS_NAME + "\n"
			+ ".super java/lang/Object\n" + "; default constructor\n" + ".method public <init>()V\n"
			+ "   aload_0 ; push this\n" + "   invokespecial java/lang/Object/<init>()V ; call super\n" + "   return\n"
			+ ".end method\n";;

	public static String PROGRAM_NAME = "CS153FunctionallyRecursive";

	public static final String MAIN_FUNCTION_DEC = ".method public static main([Ljava/lang/String;)V\n";
	public static final String FUNCTION_END = ".end method\n\n";

	public CodeGenerator(ICodeNode root, SymTabStack symTabStack, ErrorMessageHandler errors) {
		this.root = root;
		this.errors = errors;
		hasMain = false;
		currentStackNeeded = stackLimit = variablePosition = 0;
		variableLookupTable = new HashMap<>();
		functionLookupTable = new HashMap<>();
		labelNum = 0;

		contentsOfFile = new StringBuilder(DEFAULT_HEADER);
	}

	public void generateCode() {
		ArrayList<ICodeNode> functions = root.getChildren();

		for (ICodeNode f : functions) {
			handleFunctionNode(f);
		}
		
		if (!hasMain){
			errors.addErrorMessage(new ErrorMessage(0, "No main function, must have signature int main()"));
		}

		createOutputFile();
	}

	private void handleVarDec(ICodeNode varDec) {
		++numOfLocalVars;
		++currentStackNeeded;
		ArrayList<ICodeNode> children = varDec.getChildren();

		// if var is not assigned
		ICodeNode variable = children.get(0);

		if(variable.getType().equals(ICodeNodeTypeImpl.SCALAR_ARRAY))
		{
		   //get array size
		   ICodeNode size = children.get(1);
		   
	      Variable arr = new Variable();
	      arr.id = (String) variable.getAttribute(ICodeKeyImpl.ID);
	      arr.isArray = true;
	      String arrType = (String) variable.getAttribute(ICodeKeyImpl.TYPE);
	      
	      if(arrType == "int")
	         arr.type = ICodeNodeTypeImpl.INTEGER_CONSTANT;
	      else if(arrType == "float")
	         arr.type = ICodeNodeTypeImpl.REAL_CONSTANT;
	      else
	      {
	         String errMessage = "Array " + arr.id + " cannot be assigned incompatible type!";
	         createError(varDec, errMessage);
	      }
		   
		   ICodeNodeType size_type = handleExpression(size.getChildren().get(0));
		   if(!size_type.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT))
		   {
            createError(varDec, "Index is not an integer!");
         }
		      
		   contentsOfFile.append("newarray " + arrType + "\n");
		   contentsOfFile.append("astore " + variablePosition + "\n");
		   
		   //set stack position
	      arr.stackPosition = variablePosition;
	      // store for possible use later
	      
	      variableLookupTable.put(arr.id, arr);

	      ++variablePosition;
		}
		else
		{
   		ICodeNodeType expType = null;
   		// if variable is assigned and defined
   		if (children.size() == 2) {
   			ICodeNode expression = children.get(1);
   			
   			expType = handleExpression(expression);
   		}
   		
   		Variable v = new Variable();
         v.id = (String) variable.getAttribute(ICodeKeyImpl.ID);
         String varType = (String) variable.getAttribute(ICodeKeyImpl.TYPE);
   
   		if (varType.equals("float")) {
   			if (children.size() == 1) {
   				contentsOfFile.append("ldc 0.0\n");
   			}
   			v.type = ICodeNodeTypeImpl.REAL_CONSTANT;
   			contentsOfFile.append("fstore " + variablePosition + "\n");
   		} 
   		else if (varType.equals("int")) {
   			if (children.size() == 1) {
   				contentsOfFile.append("ldc 0\n");
   			}
   			v.type = ICodeNodeTypeImpl.INTEGER_CONSTANT;
   			contentsOfFile.append("istore " + variablePosition + "\n");
   		} 
   		else if (varType.equals("string")) {
   			if (children.size() == 1) {
   				contentsOfFile.append("ldc \"\"\n");
   			}
   			v.type = ICodeNodeTypeImpl.STRING_CONSTANT;
   			contentsOfFile.append("astore " + variablePosition + "\n");
   		}
   		if (expType != null) {
   			if (!expType.equals(v.type)) {
   				String errMessage = "Variable " + v.id + " cannot be assigned incompatible value!";
   				createError(varDec, errMessage);
   			}
   		}
   		//set stack position
   		v.stackPosition = variablePosition;
   		// store for possible use later
   		
   		variableLookupTable.put(v.id, v);
   
   		++variablePosition;
		}
	}

	private ICodeNodeType handleExpression(ICodeNode expression) {
		ICodeNodeType type = expression.getType();
		ICodeNodeType returnType = null;
		currentStackNeeded++;
		if (type.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT) || type.equals(ICodeNodeTypeImpl.REAL_CONSTANT)
				|| type.equals(ICodeNodeTypeImpl.STRING_CONSTANT)) {
			String const_val = (String) expression.getAttribute(ICodeKeyImpl.VALUE);
			contentsOfFile.append("ldc " + const_val + "\n");
			returnType = type;

		}
		
		//functions
		else if (type.equals(ICodeNodeTypeImpl.FUNCTION_CALL)){
			String id = (String) expression.getAttribute(ICodeKeyImpl.ID);
			Function f = functionLookupTable.get(id);
			
			returnType = f.returnType;
			
			if (returnType.equals(ICodeNodeTypeImpl.VOID)){
				this.createError(expression, "Void type functions cannot be used in expressions");
			}
			
			handleFunctionCall(expression);
		}
		
		else if (type.equals(ICodeNodeTypeImpl.VARIABLE)){
			String id = (String) expression.getAttribute(ICodeKeyImpl.ID);
			Variable v = variableLookupTable.get(id);
			
			if (v != null) {
			   returnType = v.type;
			   if(v.isArray)
			      contentsOfFile.append("aload " + v.stackPosition + "\n");
			   else
			   {
	            if (returnType.equals(ICodeNodeTypeImpl.REAL_CONSTANT)) {
	               contentsOfFile.append("fload " + v.stackPosition + "\n");
	            } else if (returnType.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)) {
	               contentsOfFile.append("iload " + v.stackPosition + "\n");
	            } else if (returnType.equals(ICodeNodeTypeImpl.STRING_CONSTANT)){
	               contentsOfFile.append("aload " + v.stackPosition + "\n");
	            }
			   }	
			} else {
				createError(expression, "Variable has not been declared yet!");
			}
		
		}
		
		else if(type.equals(ICodeNodeTypeImpl.SCALAR_ARRAY))
		{
		   String id = (String) expression.getAttribute(ICodeKeyImpl.ID);
         Variable arr = variableLookupTable.get(id);
         ICodeNode arr_ref = expression.getChildren().get(0).getChildren().get(0);

         if (arr != null && arr.isArray) {
            contentsOfFile.append("aload " + arr.stackPosition + "\n");
            returnType = arr.type;
            
            ICodeNodeType ref_type = handleExpression(arr_ref);
            
            if(!ref_type.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT))
            {
              createError(expression, "Index is not an integer!" + expression.getType().toString());
            }

            if (returnType.equals(ICodeNodeTypeImpl.REAL_CONSTANT)) {
               contentsOfFile.append("f");
            } else if (returnType.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)) {
               contentsOfFile.append("i");
            }else if (returnType.equals(ICodeNodeTypeImpl.STRING_CONSTANT)){
               contentsOfFile.append("a");
            }
            
            contentsOfFile.append("aload\n");
         } 
         else 
         {
            createError(expression, "Array has not been declared yet!");
         }
		}

		else {

			if (!type.equals(ICodeNodeTypeImpl.VARIABLE)) {
				currentStackNeeded += 2;
				ArrayList<ICodeNode> children = expression.getChildren();

				ICodeNodeType childType1 = returnType = handleExpression(children.get(0));
				ICodeNodeType childType2 = handleExpression(children.get(1));

				if (!childType1.equals(childType2)) {
					createError(expression, "Mixing data types in expressions is not permitted!");
				}

				String operationType = "i";

				if (returnType.equals(ICodeNodeTypeImpl.REAL_CONSTANT))
					operationType = "f";

				if (type.equals(ICodeNodeTypeImpl.ADD)) {
					contentsOfFile.append(operationType + "add\n");
				}

				else if (type.equals(ICodeNodeTypeImpl.SUBTRACT)) {
					contentsOfFile.append(operationType + "sub\n");
				}

				else if (type.equals(ICodeNodeTypeImpl.MULTIPLY)) {
					contentsOfFile.append(operationType + "mul\n");
				}

				else if (type.equals(ICodeNodeTypeImpl.DIVIDE)) {
					contentsOfFile.append(operationType + "div\n");
				}
				
				else if (type.equals(ICodeNodeTypeImpl.MOD)){
					if (!returnType.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)) {
						createError(expression, "Modulus can only be preformed on integers!");
					}else{
						contentsOfFile.append("irem\n");
					}
				}
				else if (LOGICAL_OPERATORS.contains(type)){
					if (!returnType.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)) {
						createError(expression, "AND and OR can only be preformed on integers!");
					}else if (type.equals(ICodeNodeTypeImpl.AND)){
						contentsOfFile.append("iand\n");
					}else if (type.equals(ICodeNodeTypeImpl.OR)){
						contentsOfFile.append("ior\n");
					}
				}
				//integer comparison conditionals
				else if (returnType.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)) {
					contentsOfFile.append(";conditionstatement\n");
					int ifTrue = getLabel();
					if (type.equals(ICodeNodeTypeImpl.EQ)) {
						contentsOfFile.append("if_icmpeq Label" + ifTrue + "\n");
					} else if (type.equals(ICodeNodeTypeImpl.GE)) {
						contentsOfFile.append("if_icmpge Label" + ifTrue + "\n");
					} else if (type.equals(ICodeNodeTypeImpl.GT)) {
						contentsOfFile.append("if_icmpgt Label" + ifTrue + "\n");

					} else if (type.equals(ICodeNodeTypeImpl.LE)) {
						contentsOfFile.append("if_icmple Label" + ifTrue + "\n");
					} else if (type.equals(ICodeNodeTypeImpl.LT)) {
						contentsOfFile.append("if_icmplt Label" + ifTrue + "\n");
					} else if (type.equals(ICodeNodeTypeImpl.NE)) {
						contentsOfFile.append("if_icmpne Label" + ifTrue + "\n");
					}

					int falseControl = getLabel();
					// set to 0 if false
					contentsOfFile.append("ldc 0\n");
					contentsOfFile.append("goto Label" + falseControl+ "\n");
					contentsOfFile.append("Label" + ifTrue + ":\n");
					// set to 1 if true
					contentsOfFile.append("ldc 1\n");
					// jump here if false
					contentsOfFile.append("Label" + falseControl + ":\n");
				}
				//floating point comparison
				else if (returnType.equals(ICodeNodeTypeImpl.REAL_CONSTANT)){
					contentsOfFile.append(";conditionstatement\n");
					contentsOfFile.append("fcmpg\n");
					int ifTrue = getLabel();
					if (type.equals(ICodeNodeTypeImpl.EQ)) {
						contentsOfFile.append("ifeq Label" + ifTrue + "\n");
					} else if (type.equals(ICodeNodeTypeImpl.GE)) {
						contentsOfFile.append("ifge Label" + ifTrue + "\n");
					} else if (type.equals(ICodeNodeTypeImpl.GT)) {
						contentsOfFile.append("ifgt Label" + ifTrue + "\n");

					} else if (type.equals(ICodeNodeTypeImpl.LE)) {
						contentsOfFile.append("ifle Label" + ifTrue + "\n");
					} else if (type.equals(ICodeNodeTypeImpl.LT)) {
						contentsOfFile.append("iflt Label" + ifTrue + "\n");
					} else if (type.equals(ICodeNodeTypeImpl.NE)) {
						contentsOfFile.append("ifne Label" + ifTrue + "\n");
					}
					int falseControl = getLabel();
					// set to 0 if false
					contentsOfFile.append("ldc 0\n");
					contentsOfFile.append("goto Label" + falseControl + "\n");
					contentsOfFile.append("Label" + ifTrue + ":\n");
					// set to 1 if true
					contentsOfFile.append("ldc 1\n");
					// jump here if false
					contentsOfFile.append("Label" + falseControl + ":\n");
					
					//expression now returns integer type
					//since float comparison returns integers
					returnType = ICodeNodeTypeImpl.INTEGER_CONSTANT;
				}
				setStackLimit();
				currentStackNeeded -= 2;
				
			}
		}

		return returnType;
	}

	private void createError(ICodeNode node, String errorMessage) {
		int lineNum = (Integer) node.getAttribute(ICodeKeyImpl.LINE);
		errors.addErrorMessage(new ErrorMessage(lineNum, errorMessage));
	}

	private void handleBlock(ICodeNode block) {

		ArrayList<ICodeNode> children = block.getChildren();

		for (ICodeNode child : children) {
			ICodeNodeTypeImpl type = (ICodeNodeTypeImpl) child.getType();
			switch (type) {
			case VAR_DECLARE:
				contentsOfFile.append(";var dec\n");
				handleVarDec(child);
				break;
			case IFELSE:
				contentsOfFile.append(";ifelse\n");
				handleIfElse(child);
				break;
			case ASSIGN:
				contentsOfFile.append(";assign\n");
				handleAssign(child);
				break;
			case WHILE:
				contentsOfFile.append(";while\n");
				handleWhile(child);
				break;
			case DO_WHILE:
				contentsOfFile.append(";dowhile\n");
				handleDoWhile(child);
				break;
			case FOR:
				contentsOfFile.append(";for\n");
				handleFor(child);
				break;
			case FUNCTION_CALL:
				contentsOfFile.append(";functioncall\n");
				handleFunctionCall(child);
				break;
			case RETURN:
				contentsOfFile.append(";return\n");
				handleReturn(child);
				break;
			case PRINTF:
				contentsOfFile.append(";printf\n");
				handlePrintf(child);
				break;
			default:
				createError(child, "Invalid Statement Type");
				break;
			}
		}

	}
	
	private void handlePrintf(ICodeNode printf){
		//only child is parameter node
		ICodeNode parmNode = printf.getChildren().get(0);
		//load first parameter on stack
		//get first child
		ArrayList<ICodeNode> parms = parmNode.getChildren();
		
		if (parms.size() >= 1){
			ICodeNode stringArg = parms.get(0);
			++currentStackNeeded;
			contentsOfFile.append("getstatic java/lang/System/out Ljava/io/PrintStream;\n");
			ICodeNodeType argType = handleExpression(stringArg);
			if (!argType.equals(ICodeNodeTypeImpl.STRING_CONSTANT)){
				createError(stringArg, "First argument to printf must be a string!");
			}
			createArgsForPrintf(parms);
		}else{
			createError(parmNode, "Must provide at least one argument, a string, to printf");
		}
	}
	
	private void createArgsForPrintf(ArrayList<ICodeNode> printfArgs){
		int arrSize = printfArgs.size() - 1;
		
		contentsOfFile.append("ldc " + arrSize + "\n");
		contentsOfFile.append("anewarray java/lang/Object\n");
		currentStackNeeded += 2;
		//skip first arg
		for (int i = 1; i < printfArgs.size(); ++i){
			currentStackNeeded += 2;
			contentsOfFile.append("dup\n");
			contentsOfFile.append("ldc " + (i - 1) + "\n");
			ICodeNodeType type = handleExpression(printfArgs.get(i));
			//store index number on stack
			if (type.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
				contentsOfFile.append("invokestatic  java/lang/Integer.valueOf(I)Ljava/lang/Integer;\n");
			}else if (type.equals(ICodeNodeTypeImpl.REAL_CONSTANT)){
				contentsOfFile.append("invokestatic  java/lang/Float.valueOf(F)Ljava/lang/Float;\n");
			}else if (type.equals(ICodeNodeTypeImpl.STRING_CONSTANT)){
				//no code needed
			}
			contentsOfFile.append("aastore\n");
			
		}
		contentsOfFile.append("invokevirtual java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;\n");
		contentsOfFile.append("pop\n");
	}
	
	private void handleReturn(ICodeNode returnNode){
		ArrayList<ICodeNode> children = returnNode.getChildren();
		
		handleRefVarsForReturn(currentFunction);
		//no children if void return
		if (children.isEmpty()){
			contentsOfFile.append("return\n");
		}else{
			
			//first child is expression
			ICodeNodeType type = handleExpression(children.get(0));
			
			if (type.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
				contentsOfFile.append('i');
			}else if (type.equals(ICodeNodeTypeImpl.REAL_CONSTANT)){
				contentsOfFile.append('f');
			}else if (type.equals(ICodeNodeTypeImpl.STRING_CONSTANT)){
				contentsOfFile.append('a');
			}
			
			contentsOfFile.append("return\n");
		}
	}
	
	private void handleRefVarsForReturn(Function f){
		ArrayList<Parameter> parms = f.parameters;
		
		for (Parameter parm : parms){
			if (parm.referenceVariable){
				contentsOfFile.append("aload " + parm.localVarPosition + "\n");
				int localVarPosition = variableLookupTable.get(parm.id).stackPosition;
				
				if (parm.type.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
					contentsOfFile.append('i');
				}else if (parm.type.equals(ICodeNodeTypeImpl.REAL_CONSTANT)){
					contentsOfFile.append('f');
				}else if (parm.type.equals(ICodeNodeTypeImpl.STRING_CONSTANT)){
					contentsOfFile.append('a');
				}
				
				contentsOfFile.append("load " + localVarPosition + "\n");
				
				if (parm.type.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
					contentsOfFile.append("putfield runtime/IWrap/val I\n");
				}else if (parm.type.equals(ICodeNodeTypeImpl.REAL_CONSTANT)){
					contentsOfFile.append("putfield runtime/FWrap/val F\n");
				}else if (parm.type.equals(ICodeNodeTypeImpl.STRING_CONSTANT)){
					contentsOfFile.append("putfield runtime/StrWrap/val Ljava/lang/String;\n");
				}
			}
		}
	}
	
	private void handleFunctionCall(ICodeNode functCall){
		
		//make sure function has been defined
		String functName = (String) functCall.getAttribute(ICodeKeyImpl.ID);
		
		Function funct = functionLookupTable.get(functName);
		
		if (funct == null){
			createError(functCall, "Function is being used before it is defined");
		}else{
			ArrayList<ICodeNode> children = functCall.getChildren();
			
			//only child are the parameters
			ICodeNode parameterBlock = children.get(0);
			
			ArrayList<ICodeNode> parameters = parameterBlock.getChildren();
			
			//make sure called parameters are the same length as the defined parameter list
			if (parameters.size() == funct.parameters.size()){
				//evaluate parameters and check to make sure their types are correct
				for (int i = 0; i < parameters.size(); ++i){
					ICodeNodeType type = handleVariableWrapping(funct.parameters.get(i), parameters.get(i));
					
					if (!type.equals(funct.parameters.get(i).type)){
						createError(functCall, "Function is being called with invalid typed parameters");
						return;
					}
					
				}
				
				//call function
				contentsOfFile.append(String.format("invokestatic %s/%s(%s)%s\n", CLASS_NAME, functName,
						funct.parameterTypesString, funct.returnTypeString));
				
				//handle unwrapping
				handleFunctCallUnwrapping(funct.parameters, parameters);
				
			}else{
				createError(functCall, "Function is being called with an invalid number of arguments");
			}
		}
	}
	
	private void handleFunctCallUnwrapping(ArrayList<Parameter> parms, 
			ArrayList<ICodeNode> params){
		
		for (int i = 0; i < params.size(); ++i){
			ICodeNode var = params.get(i);
			ICodeNodeType type = var.getType();
			if (type.equals(ICodeNodeTypeImpl.VARIABLE)){
				Parameter p = parms.get(i);
				if (p.referenceVariable){
					//load wrapped address value
					contentsOfFile.append("aload " +p.callingFunctionPosition+"\n");
					int originalVarPosition = variableLookupTable.get(var.getAttribute(ICodeKeyImpl.ID)).stackPosition;
					if (p.type.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
						contentsOfFile.append("getfield runtime/IWrap/val I\n");
						contentsOfFile.append("istore " + originalVarPosition + "\n");
					}else if (p.type.equals(ICodeNodeTypeImpl.REAL_CONSTANT)){
						contentsOfFile.append("getfield runtime/FWrap/val F\n");
						contentsOfFile.append("fstore " + originalVarPosition + "\n");
					}else if (p.type.equals(ICodeNodeTypeImpl.STRING_CONSTANT)){
						contentsOfFile.append("getfield runtime/StrWrap/val Ljava/lang/String;\n");
						contentsOfFile.append("astore " + originalVarPosition + "\n");
					}
				}
			}
		}
		
	}
	
	private ICodeNodeType handleVariableWrapping(Parameter parm, ICodeNode variable){
		ICodeNodeType type = null;
		if (parm.referenceVariable){
			if (variable.getType().equals(ICodeNodeTypeImpl.VARIABLE)){
				type = parm.type;
				//for storing wrapped value
				++this.numOfLocalVars;
				int varPosition = this.variableLookupTable.get(variable.getAttribute(ICodeKeyImpl.ID)).stackPosition;
				if (type.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
					contentsOfFile.append("new runtime/IWrap\n");
					contentsOfFile.append("dup\n");
					contentsOfFile.append("iload "+varPosition+"\n");
					contentsOfFile.append("invokenonvirtual runtime/IWrap/<init>(I)V\n");
				}else if (type.equals(ICodeNodeTypeImpl.REAL_CONSTANT)){
					contentsOfFile.append("new runtime/FWrap\n");
					contentsOfFile.append("dup\n");
					contentsOfFile.append("fload "+varPosition+"\n");
					contentsOfFile.append("invokenonvirtual runtime/FWrap/<init>(F)V\n");
				}else if (type.equals(ICodeNodeTypeImpl.STRING_CONSTANT)){
					contentsOfFile.append("new runtime/StrWrap\n");
					contentsOfFile.append("dup\n");
					contentsOfFile.append("aload "+varPosition+"\n");
					contentsOfFile.append("invokenonvirtual runtime/StrWrap/<init>(Ljava/lang/String;)V\n");
				}
				
				contentsOfFile.append("dup\n");
				
				parm.callingFunctionPosition = this.variablePosition;
				//store wrapped value in temp position to be unwrapped after function call
				contentsOfFile.append("astore "+this.variablePosition+"\n");
				
				variablePosition++;
			}else{
				type = parm.type;
				//for storing wrapped value
				if (type.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
					contentsOfFile.append("new runtime/IWrap\n");
					contentsOfFile.append("dup\n");
					type = handleExpression(variable);
					contentsOfFile.append("invokenonvirtual runtime/IWrap/<init>(I)V\n");
				}else if (type.equals(ICodeNodeTypeImpl.REAL_CONSTANT)){
					contentsOfFile.append("new runtime/FWrap\n");
					contentsOfFile.append("dup\n");
					type = handleExpression(variable);
					contentsOfFile.append("invokenonvirtual runtime/FWrap/<init>(F)V\n");
				}else if (type.equals(ICodeNodeTypeImpl.STRING_CONSTANT)){
					contentsOfFile.append("new runtime/StrWrap\n");
					contentsOfFile.append("dup\n");
					type = handleExpression(variable);
					contentsOfFile.append("invokenonvirtual runtime/StrWrap/<init>(Ljava/lang/String;)V\n");
				}
				
				
			}
		}else{
			type = handleExpression(variable);
		}
		
		return type;
	}
	
	private void handleFor(ICodeNode forStat) {
		ArrayList<ICodeNode> children = forStat.getChildren();
		
		//handle variable init statement if exists
		ICodeNode forInit = children.get(0);
		ArrayList<ICodeNode> initChildren = forInit.getChildren();
		if (initChildren.size() == 1){
			handleAssign(initChildren.get(0));
		}
		
		//second child is the condition expression
		ICodeNode condition = children.get(1);
		
		//condition only has one child, an expression
		ICodeNode conditionExpr = condition.getChildren().get(0);
		//create label to jump to for looping
		int topLabel = getLabel();
		contentsOfFile.append("Label" + topLabel + ":\n");
		ICodeNodeType exprType = handleExpression(conditionExpr);
		
		if (!exprType.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
			createError(conditionExpr, "Conditional expressions must be of type integer!");
		}
		int bottomLabel = getLabel();
		// jump to end if condition expression is equal to zero
		contentsOfFile.append(";For condition control\n");
		contentsOfFile.append("ifeq Label" + bottomLabel + "\n");
		//handle block statement
		ICodeNode block = children.get(3);
		handleBlock(block);
	
		//handle iteration assignment statement if exists
		ICodeNode forItr = children.get(2);
		ArrayList<ICodeNode> itrChildren = forItr.getChildren();
		if (itrChildren.size() == 1){
			handleAssign(itrChildren.get(0));
		}
		
		//goto top for check
		contentsOfFile.append("goto Label" + topLabel + "\n");
		//label to jump to stop looping
		contentsOfFile.append("Label" + bottomLabel + ":\n");
	}

	private void handleWhile(ICodeNode whileStat) {
		ArrayList<ICodeNode> children = whileStat.getChildren();
		
		//first child is the condition expression
		ICodeNode condition = children.get(0);
		
		//condition only has one child, an expression
		ICodeNode conditionExpr = condition.getChildren().get(0);
		//create label to jump to for looping
		int topLabel = getLabel();
		contentsOfFile.append("Label" + topLabel + ":\n");
		ICodeNodeType exprType = handleExpression(conditionExpr);
		
		if (!exprType.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
			createError(conditionExpr, "Conditional expressions must be of type integer!");
		}
		int bottomLabel = getLabel();
		// jump to end if condition expression is equal to zero
		contentsOfFile.append("ifeq Label" + bottomLabel + "\n");
		//handle block statement
		ICodeNode block = children.get(1);
		handleBlock(block);
		//goto top for check
		contentsOfFile.append("goto Label" + topLabel + "\n");
		//label to jump to stop looping
		contentsOfFile.append("Label" + bottomLabel + ":\n");
	}
	
	private void handleDoWhile(ICodeNode doWhile){
		ArrayList<ICodeNode> children = doWhile.getChildren();
		
		//create label to jump to for looping
		int topLabel = getLabel();
		contentsOfFile.append("Label" + topLabel + ":\n");
		//handle block statement
		ICodeNode block = children.get(1);
		handleBlock(block);
		
		//first child is the condition expression
		ICodeNode condition = children.get(0);
		
		//condition only has one child, an expression
		ICodeNode conditionExpr = condition.getChildren().get(0);
		ICodeNodeType exprType = handleExpression(conditionExpr);
		
		if (!exprType.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
			createError(conditionExpr, "Conditional expressions must be of type integer!");
		}
		// jump to top if condition expression is not equal to zero
		contentsOfFile.append("ifne Label" + topLabel + "\n");
	}

	private void handleAssign(ICodeNode assign) {
		ArrayList<ICodeNode> children = assign.getChildren();

		String id = (String) assign.getAttribute(ICodeKeyImpl.ID);
		Variable v = variableLookupTable.get(id);
		
	  // two children, second child is an array reference
      if(children.size() == 2)
      {
         ICodeNode arr_ref = children.get(1);
         contentsOfFile.append("aload " + v.stackPosition + "\n");
         ICodeNodeType index_exp = handleExpression(arr_ref.getChildren().get(0));
         
         if(!index_exp.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT))
         {
            createError(assign, "Index is not an integer! " + index_exp.toString());
         }
      }

		// only one child, the expression it is assigned to
		ICodeNode expr = children.get(0);
		ICodeNodeType exprType = handleExpression(expr);
		
		String loadExtension = "i";
		if (v.type.equals(exprType)) {
        if (v.type.equals(ICodeNodeTypeImpl.REAL_CONSTANT)) {
            loadExtension = "f";
        }
        else if (v.type.equals(ICodeNodeTypeImpl.STRING_CONSTANT)) {
            loadExtension = "a";
        }
      } else {
         createError(assign, "Incompatible type for assignment");
      }
		
		if(children.size() == 2){
         contentsOfFile.append(loadExtension + "astore\n");
		}		
		else
		   contentsOfFile.append(loadExtension + "store " + v.stackPosition + "\n");
	}

	private void handleIfElse(ICodeNode ifElse) {
		ArrayList<ICodeNode> children = ifElse.getChildren();

		// if statement
		ICodeNode ifStatement = children.get(0);

		ArrayList<ICodeNode> ifChildren = ifStatement.getChildren();
		// deal with condition statement
		ICodeNode expr = ifChildren.get(0).getChildren().get(0);
		ICodeNodeType exprType = handleExpression(expr);
		
		if (!exprType.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
			createError(expr, "Conditional expressions must be of type integer!");
		}
		// jump if condition expression is equal to zero
		int elseLabelNum = getLabel();
		int endOfElseNum = getLabel();
		contentsOfFile.append("ifeq Label" + elseLabelNum + "\n");
		// deal with block of code in if statement
		handleBlock(ifChildren.get(1));
		// goto end of else statement

		contentsOfFile.append("goto Label" + endOfElseNum + "\n");
		contentsOfFile.append("Label" + elseLabelNum + ":\n");
		// else statement
		if (children.size() == 2) {
			ICodeNode elseStatement = children.get(1);
			handleBlock(elseStatement.getChildren().get(0));

		}
		contentsOfFile.append("Label" + endOfElseNum + ":\n");
		// jump to this and end of if statement
	}
	
	/*
	 * Helper function, call this if you need a label number for something
	 */
	private int getLabel(){
		return labelNum++;
	}

	private void handleFunctionNode(ICodeNode function) {
		ICodeNodeType type = function.getType();
		numOfLocalVars = currentStackNeeded = variablePosition = 0;
		stackLimit = currentStackNeeded = 2;
		if (type.equals(ICodeNodeTypeImpl.MAIN_FUNCTION)) {
			hasMain = true;
			contentsOfFile.append(MAIN_FUNCTION_DEC);
			
			ICodeNode block = function.getChildren().get(1);
			contentsOfFile.append("#placeholder#\n");
			handleBlock(block);

		
			contentsOfFile.append("return\n");
			contentsOfFile.append(FUNCTION_END);

		}
		else if(type.equals(ICodeNodeTypeImpl.FUNCTION_DEC)) {
			String functionName = (String)function.getAttribute(ICodeKeyImpl.ID);
			Function f = new Function();
			currentFunction = f;
			f.id = functionName;
			functionLookupTable.put(functionName, f);
		   contentsOfFile.append(".method public static " 
		         + functionName + "(");
		   
		   ICodeNode parameters = function.getChildren().get(0);
		   
		   handleParameterNode(parameters, f);
		   
		   String return_type = (String) function.getAttribute(ICodeKeyImpl.RETURN_TYPE);
		   if (return_type.equals("float")) {
            contentsOfFile.append(")F\n");
            f.returnTypeString = "F";
            f.returnType = ICodeNodeTypeImpl.REAL_CONSTANT;
         }
		   else if (return_type.equals("void")){
		      contentsOfFile.append(")V\n");
		      f.returnTypeString = "V";
		      f.returnType = ICodeNodeTypeImpl.VOID;
		   }
         else if (return_type.equals("int")) {
            contentsOfFile.append(")I\n");
            f.returnTypeString = "I";
            f.returnType = ICodeNodeTypeImpl.INTEGER_CONSTANT;
         }
         else if (return_type.equals("string")) {
            contentsOfFile.append(")Ljava/lang/String;\n");
            f.returnTypeString = "Ljava/lang/String;";
            f.returnType = ICodeNodeTypeImpl.STRING_CONSTANT;
         }
         else
         {
            createError(parameters, "Function " + 
                  function.getAttribute(ICodeKeyImpl.ID) +  
                  "does not have a recognizable return type!");
            return;
         }
		   contentsOfFile.append("#placeholder#\n");
		 //handle code for unwrapping variables  
		 handleVariableUnwrapping(f);
		 ICodeNode block = function.getChildren().get(1);
         
         
         handleBlock(block);
         
         //in case of implicit return
         handleRefVarsForReturn(currentFunction);
         
         //set default return value if needed
         if (!f.returnTypeString.equals("V")){
        	 if (f.returnType.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
        		 contentsOfFile.append("ldc 0\n");
        		 contentsOfFile.append(f.returnTypeString.toLowerCase());
        	 }
        	 else if (f.returnType.equals(ICodeNodeTypeImpl.REAL_CONSTANT)){
        		 contentsOfFile.append("ldc 0.0\n");
        		 contentsOfFile.append(f.returnTypeString.toLowerCase());
        	 }else if (f.returnType.equals(ICodeNodeTypeImpl.STRING_CONSTANT)){
        		 contentsOfFile.append("ldc \"\"\n");
        		 contentsOfFile.append("a");
        	 }
         }
         contentsOfFile.append("return\n");
         contentsOfFile.append(FUNCTION_END);

		}
		String contentsSoFar = contentsOfFile.toString();
		
		setStackLimit();
		// insert stack limit and local var limit
		contentsSoFar = contentsSoFar.replace("#placeholder#\n", 
		String.format(".limit stack %d\n.limit locals %d\n", stackLimit, numOfLocalVars+1));
		contentsOfFile = new StringBuilder(contentsSoFar); 	
		currentFunction = null;
	}
	
	private void handleVariableUnwrapping(Function f){
		ArrayList<Parameter> parms = f.parameters;
		
		for (Parameter p : parms){
			if (p.referenceVariable){
				currentStackNeeded += 2;
				++numOfLocalVars;
				Variable v = variableLookupTable.get(p.id);
				
				//unwrap and store variable in new spot
				contentsOfFile.append("aload "+v.stackPosition+ "\n");
				
				if (p.type.equals(ICodeNodeTypeImpl.INTEGER_CONSTANT)){
					contentsOfFile.append("getfield runtime/IWrap/val I\n");
					contentsOfFile.append("istore " + variablePosition+ "\n");
				}else if (p.type.equals(ICodeNodeTypeImpl.REAL_CONSTANT)){
					contentsOfFile.append("getfield runtime/FWrap/val F\n");
					contentsOfFile.append("fstore " + variablePosition+ "\n");
				}
				else if (p.type.equals(ICodeNodeTypeImpl.STRING_CONSTANT)){
					contentsOfFile.append("getfield runtime/StrWrap/val Ljava/lang/String;\n");
					contentsOfFile.append("astore " + variablePosition + "\n");
				}
				
				//update variable id
				v.stackPosition = variablePosition;
				++variablePosition;
				++numOfLocalVars;
				
				setStackLimit();
				
				currentStackNeeded -= 2;
			}
		}
		
	}
	
	private void setStackLimit(){
		if (stackLimit < currentStackNeeded){
			stackLimit = currentStackNeeded;
		}
	}
	
	private void handleParameterNode(ICodeNode parameters, Function f) {
	   
	   ArrayList<ICodeNode> param_list = parameters.getChildren();
	   
	   ArrayList<Function.Parameter> paramTypes = new ArrayList<>();
	   
	   String parameterTypesString = "";
	   
	   for(ICodeNode params : param_list)
	   {
	      ++numOfLocalVars;
	      Variable v = new Variable();
	      v.id = (String) params.getAttribute(ICodeKeyImpl.ID);
	      String varType = (String) params.getAttribute(ICodeKeyImpl.TYPE);
	      boolean referenceVar = (Boolean) params.getAttribute(ICodeKeyImpl.REFERENCE);
	      
	      if (varType.equals("float")) {
	    	 if (referenceVar){
	    		 parameterTypesString += "Lruntime/FWrap;";
	    	 }
	    	 else if(params.getType().equals(ICodeNodeTypeImpl.SCALAR_ARRAY))
	    	 {
	    		 v.isArray = true;
	    	    parameterTypesString += "[F";
	    	 }
	    	 else
	    	 {
	    		 parameterTypesString += "F";
	    	 }
	         v.type = ICodeNodeTypeImpl.REAL_CONSTANT;
	      }
	      else if (varType.equals("int")) {
	    	 if (referenceVar){
	    		 parameterTypesString += "Lruntime/IWrap;";
	    	 }
	    	 else if(params.getType().equals(ICodeNodeTypeImpl.SCALAR_ARRAY))
          {
	    		 v.isArray = true;
             parameterTypesString += "[I";
          }
	    	 else
	    	 {
	    		 parameterTypesString += "I";
	    	 }
	         v.type = ICodeNodeTypeImpl.INTEGER_CONSTANT;
	      }
	      else if (varType.equals("string")) {
    	     if (referenceVar){
	    		 parameterTypesString += "Lruntime/StrWrap;";
	    	 }else{
	    		 parameterTypesString += "Ljava/lang/String;";
	    	 }
	         v.type = ICodeNodeTypeImpl.STRING_CONSTANT;
	      }
	      Parameter p = new Parameter(v.type, referenceVar, v.id);
	      paramTypes.add(p);
	      
	      v.stackPosition = p.localVarPosition = variablePosition;
	      variableLookupTable.put(v.id, v);
	      ++variablePosition;
	   }
	   
	   f.parameters = paramTypes;
	   f.parameterTypesString = parameterTypesString;
	   
	   //append string for params
	   contentsOfFile.append(f.parameterTypesString);
	}

	/**
	 * Dumps contends of StringBuilder object to the output file named
	 * OUTPUT_FILE_NAME
	 */

	private void createOutputFile() {
		if (!errors.areThereErrors()) {
			File outputFile = new File(OUTPUT_FILE_PATH);

			if (outputFile.exists())
				outputFile.delete();

			try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));) {
				writer.write(contentsOfFile.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}

			System.out.println("=================");
			System.out.println("Code Generated Successfully");
			System.out.println("Output File: " + OUTPUT_FILE_PATH);
			System.out.println("=================");
			System.out.println(contentsOfFile);
			System.out.println("=================");
		}
	}
}
