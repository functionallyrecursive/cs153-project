package codegen;

import intermediate.ICodeNodeType;
import intermediate.types.TypeImpl;

public class Variable {
	public ICodeNodeType type;
	public String id;
	public boolean isArray = false;
	public int stackPosition;
}
