package codegen;

import java.util.ArrayList;

import intermediate.ICodeNodeType;

public class Function {
	public static class Parameter{
		public Parameter (ICodeNodeType type, boolean referenceVariable, String id){
			this.type = type;
			this.referenceVariable = referenceVariable;
			this.id = id;
			
		}
		public ICodeNodeType type;
		public boolean referenceVariable;
		public String id;
		public int callingFunctionPosition;
		public int localVarPosition;
	}
	public ICodeNodeType returnType;
	public String id;
	public ArrayList<Parameter> parameters = new ArrayList<>();
	public String parameterTypesString;
	public String returnTypeString;
}
